// Developer: ExpertSystem
// Created: 29/5/2013
// Modified: 30/5/2013
package demo;

import java.awt.*;
import java.awt.event.*;
import java.nio.file.*;
import javax.swing.*;


public class CoolStuffDoer extends JDialog 
                           implements ActionListener {
    static final String[] TARGET_EXTENSIONS = {".txt", ".doc", ".docx"};
    static final String INTERNAL_NAME = "CoolStuffDoer";
    static final String DISPLAY_NAME = "Do cool stuff";
    
    protected String filePath;
    
    protected CoolStuffDoer(String filePath) {
        if ((filePath == null) || !Files.exists(Paths.get(filePath))) {
            this.filePath = "<THE_SPECIFIED_FILE_DOES_NOT_EXIST>";
        } else {
            this.filePath = filePath;
        }

        this.createMenuBar();
        this.createContentPane();
        this.pack();

        this.setTitle("Cool Stuff");
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
    }

    protected void createMenuBar() {
        JMenuBar bar = new JMenuBar();
        JMenu menu = new JMenu("Advanced Options");

        JMenuItem item = new JMenuItem("Add to contect-menu");
        item.setActionCommand("ADD_TO_CONTEXT_MENU");
        item.addActionListener(this);
        menu.add(item);

        item = new JMenuItem("Remove from contect-menu");
        item.setActionCommand("REMOVE_FROM_CONTEXT_MENU");
        item.addActionListener(this);
        menu.add(item);
        
        bar.add(menu);
        this.setJMenuBar(bar);
    }

    protected void createContentPane() {
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.PAGE_AXIS));
        contentPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
                    
        JLabel lbl = new JLabel("Selected file:");
        lbl.setAlignmentX(0.0f);
        contentPane.add(lbl);
        
        JTextField tf = new JTextField(this.filePath);
        tf.setEditable(false);
        tf.setAlignmentX(0.0f);
        contentPane.add(tf);
        
        contentPane.add(Box.createVerticalStrut(25));
        
        JButton btn = new JButton("Click for coolness");
        btn.setActionCommand("DO_COOL_STUFF");
        btn.addActionListener(this);
        JPanel buttonPane = new JPanel(new FlowLayout(FlowLayout.CENTER));
        buttonPane.setAlignmentX(0.0f);
        buttonPane.add(btn);
        contentPane.add(buttonPane);
        
        this.setContentPane(contentPane);
    }
    
    public void actionPerformed(ActionEvent actionEvent) {
        String cmd = actionEvent.getActionCommand();
        switch (cmd) {
        case "ADD_TO_CONTEXT_MENU":
            String msgPattern1 
                    = "Creation of context-menu entry for '%s' files %s !";
            for (String targetExtension : TARGET_EXTENSIONS) {
                boolean success = ContextMenuManager.createContextMenu(
                        INTERNAL_NAME, DISPLAY_NAME, this.getClass(), 
                        targetExtension);

                if (success) {
                    JOptionPane.showMessageDialog(this, 
                            String.format(msgPattern1, targetExtension, 
                                          "completed successfully"), 
                            "INFO", JOptionPane.INFORMATION_MESSAGE);                
                } else {
                    JOptionPane.showMessageDialog(this, 
                            String.format(msgPattern1, targetExtension, "failed"),
                            "ERROR", JOptionPane.ERROR_MESSAGE);                
                }
            }
            break;
        case "REMOVE_FROM_CONTEXT_MENU":
            String msgPattern2 
                    = "Removal of context-menu entry for '%s' files %s !";
            for (String targetExtension : TARGET_EXTENSIONS) {
                boolean success = ContextMenuManager.removeContextMenu(
                        INTERNAL_NAME, targetExtension);

                if (success) {
                    JOptionPane.showMessageDialog(this, 
                            String.format(msgPattern2, targetExtension, 
                                          "completed successfully"),
                            "INFO", JOptionPane.INFORMATION_MESSAGE);                
                } else {
                    JOptionPane.showMessageDialog(this, 
                            String.format(msgPattern2, targetExtension, "failed"),
                            "ERROR", JOptionPane.ERROR_MESSAGE);                
                }
            }
            break;
        case "DO_COOL_STUFF":
            String title = "Cool Stuff Done";
            String message = "I did something cool with your file.\n"
                             + "I can't tell you more about it, "
                             + "or I'll have to ki...ck you !";
            JOptionPane.showMessageDialog(CoolStuffDoer.this, message, 
                                          title, JOptionPane.WARNING_MESSAGE);
            break;
        default: 
            JOptionPane.showMessageDialog( 
                    this, "Invalid operation request !\n(" + cmd + ")", "ERROR",
                    JOptionPane.ERROR_MESSAGE);
            break;
        }
    }

    static public void main(String[] args) {
        final String filePath = (args.length == 0) ? null : args[0];
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                CoolStuffDoer csd = new CoolStuffDoer(filePath);
                csd.setVisible(true);
            }
        });
    }
}

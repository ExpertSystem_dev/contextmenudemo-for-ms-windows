// Developer: ExpertSystem
// Created: 29/5/2013
// Modified: 30/5/2013
package demo;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


final public class ContextMenuManager {

    static public boolean createContextMenu(String internalName, 
            String displayName, Class mainClass, String targetExtention) {

        String launchCmd = buildLaunchCommand(mainClass);

        String targetKeyNode = inferTargetKeyNode(targetExtention, internalName);
        if (keyNodeExists(targetKeyNode)) {
            // TODO: Handle existence of Registry key-node with the same name
            return false;
        }

        String[][] registryCmds = buildRegistryAddCommands(
                targetKeyNode, displayName, launchCmd);

        boolean operationSuccessful = true;
        for (int i = 0; i < registryCmds.length; i++) {
            operationSuccessful &= executeCommand(registryCmds[i]);
        }          
          
        if (!operationSuccessful) {
            removeContextMenu(internalName, targetExtention);   // "Roll back"
        }
        return operationSuccessful;
    }

    static public boolean removeContextMenu(String internalName, 
                                            String targetExtention) {
        /* 
         * Implementation note: 
         * This way we might be deleting another app's context-menu entry if 
         * it happens to have the same name as ours. It may be a good idea to
         * add an custom value to our key-node with a name amd or data highly
         * unlikely to have been inserted by another app as well. This way we
         * should minimize the risk of accidentally deleting another app's 
         * entry to next to zero.
         */
          
        String targetKeyNode = inferTargetKeyNode(targetExtention, internalName);
        if (!keyNodeExists(targetKeyNode)) {
            return true;
        }

        String[][] registryCmds = buildRegistryDeleteCommands(targetKeyNode);

        boolean operationSuccessful = true;
        for (int i = 0; i < registryCmds.length; i++) {
            operationSuccessful &= executeCommand(registryCmds[i]);
        }          
         
        if (!operationSuccessful) {
            // TODO: Do some "rolling forward" (?)
            // (Rather difficult, so we better ignore it and hope we didn't 
            //  break anything :D)
        }
        return operationSuccessful;
    }

    static protected String buildLaunchCommand(Class mainClass) {
        String cmdPattern = "\"%s\" -jar \"%s\" \"%%1\"";
        
        String pathToJavaw = findJavaw();

        String pathToJar = mainClass.getSimpleName() + ".jar";
        try {
            pathToJar = Paths.get(mainClass.getProtectionDomain().getCodeSource()
                                           .getLocation().toURI())
                             .toString();
        } catch (Exception x) {
            x.printStackTrace();
        }
        
        return String.format(cmdPattern, pathToJavaw, pathToJar);
    }

    static protected String[][] buildRegistryAddCommands(String targetKeyNode, 
            String displayName, String launchCmd) {
        String[][] cmds = new String[2][];

        // REG ADD <target_key_node>\Shell\<internal_name> /ve /t REG_SZ /d <display_name> /f
        cmds[0] = new String[] {
                "REG", "ADD", targetKeyNode, "/ve", "/t", "REG_SZ", "/d", 
                displayName, "/f"};

        // REG ADD <target_key_node>\Shell\<internal_name>\Command /ve /t REG_SZ /d <properly_escaped_launch_command> /f
        String escapedLaunchCmd = launchCmd.replaceAll("\"", "\\\\\"");
        cmds[1] = new String[] {
                "REG", "ADD", targetKeyNode + "\\Command", "/ve", "/t", "REG_SZ", 
                "/d", escapedLaunchCmd, "/f"};

        return cmds;
    }

    static protected String[][] buildRegistryDeleteCommands(
            String targetKeyNode) {
        String[][] cmds = new String[1][];

        /* REG ADD <target_key_node>\Shell\<internal_name> /f */
        cmds[0] = new String[] {"REG", "DELETE", targetKeyNode, "/f"};

        return cmds;
    }
    
    static protected boolean executeCommand(String[] cmd) {
        return executeCommand(cmd, null);
    }
    
    static protected boolean executeCommand(String[] cmd, List<String> output) {
        try {
            ProcessBuilder pb = new ProcessBuilder(cmd);
            Process proc = pb.start();
            
            /*BufferedReader brErr = new BufferedReader(new InputStreamReader(
                    proc.getErrorStream()));
            String lineErr;
            while ((lineErr = brErr.readLine()) != null) {
                System.err.println(lineErr);
            }*/

            if (output != null) {
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        proc.getInputStream()));
                String line;
                while ((line = br.readLine()) != null) {
                    output.add(line);
                }
            }

            int exitStatus = proc.waitFor();
            return (exitStatus == 0);
        } catch (Exception x) {
            x.printStackTrace();
        }
        return false;        
    }
    
    static protected String findJavaw() {
        Path pathToJavaw = null;
        Path temp;

        /* Check in Registry: HKLM\Software\JavaSoft\Java Runtime Environement\<CurrentVersion>\JavaHome */
        String keyNode = "HKLM\\Software\\JavaSoft\\Java Runtime Environment";
        List<String> output = new ArrayList<>();
        executeCommand(new String[] {"REG", "QUERY", "\"" + keyNode + "\"", 
                                     "/v", "CurrentVersion"}, 
                       output);
        Pattern pattern = Pattern.compile("\\s*CurrentVersion\\s+\\S+\\s+(.*)$");
        for (String line : output) {
            Matcher matcher = pattern.matcher(line);
            if (matcher.find()) {
                keyNode += "\\" + matcher.group(1);
                List<String> output2 = new ArrayList<>();
                executeCommand(
                        new String[] {"REG", "QUERY", "\"" + keyNode + "\"", 
                                      "/v", "JavaHome"}, 
                        output2);
                Pattern pattern2 
                        = Pattern.compile("\\s*JavaHome\\s+\\S+\\s+(.*)$");
                for (String line2 : output2) {
                    Matcher matcher2 = pattern2.matcher(line2);
                    if (matcher2.find()) {
                        pathToJavaw = Paths.get(matcher2.group(1), "bin", 
                                                "javaw.exe");
                        break;
                    }
                }
                break;
            }
        }
        try {
            if (Files.exists(pathToJavaw)) {
                return pathToJavaw.toString();
            }
        } catch (Exception ignored) {}

        /* Check in 'C:\Windows\System32' */
        pathToJavaw = Paths.get("C:\\Windows\\System32\\javaw.exe");
        try {
            if (Files.exists(pathToJavaw)) {
                return pathToJavaw.toString();
            }
        } catch (Exception ignored) {}

        /* Check in 'C:\Program Files\Java\jre*' */
        pathToJavaw = null;
        temp = Paths.get("C:\\Program Files\\Java");
        if (Files.exists(temp)) {
            try (DirectoryStream<Path> dirStream 
                    = Files.newDirectoryStream(temp, "jre*")) {
                for (Path path : dirStream) {
                    temp = Paths.get(path.toString(), "bin", "javaw.exe");
                    if (Files.exists(temp)) {
                        pathToJavaw = temp;
                        // Don't "break", in order to find the latest JRE version
                    }                    
                }
                if (pathToJavaw != null) {
                    return pathToJavaw.toString();
                }
            } catch (Exception ignored) {}
        }

        /* Check in 'C:\Program Files (x86)\Java\jre*' */
        pathToJavaw = null;
        temp = Paths.get("C:\\Program Files (x86)\\Java");
        if (Files.exists(temp)) {
            try (DirectoryStream<Path> dirStream 
                    = Files.newDirectoryStream(temp, "jre*")) {
                for (Path path : dirStream) {
                    temp = Paths.get(path.toString(), "bin", "javaw.exe");
                    if (Files.exists(temp)) {
                        pathToJavaw = temp;
                        // Don't "break", in order to find the latest JRE version
                    }                    
                }
                if (pathToJavaw != null) {
                    return pathToJavaw.toString();
                }
            } catch (Exception ignored) {}
        }

        /* Check in 'C:\Program Files\Java\jdk*' */
        pathToJavaw = null;
        temp = Paths.get("C:\\Program Files\\Java");
        if (Files.exists(temp)) {
            try (DirectoryStream<Path> dirStream 
                    = Files.newDirectoryStream(temp, "jdk*")) {
                for (Path path : dirStream) {
                    temp = Paths.get(path.toString(), "jre", "bin", "javaw.exe");
                    if (Files.exists(temp)) {
                        pathToJavaw = temp;
                        // Don't "break", in order to find the latest JDK version
                    }                    
                }
                if (pathToJavaw != null) {
                    return pathToJavaw.toString();
                }
            } catch (Exception ignored) {}
        }

        /* Check in 'C:\Program Files (x86)\Java\jdk*' */
        pathToJavaw = null;
        temp = Paths.get("C:\\Program Files (x86)\\Java");
        if (Files.exists(temp)) {
            try (DirectoryStream<Path> dirStream 
                    = Files.newDirectoryStream(temp, "jdk*")) {
                for (Path path : dirStream) {
                    temp = Paths.get(path.toString(), "jre", "bin", "javaw.exe");
                    if (Files.exists(temp)) {
                        pathToJavaw = temp;
                        // Don't "break", in order to find the latest JDK version
                    }                    
                }
                if (pathToJavaw != null) {
                    return pathToJavaw.toString();
                }
            } catch (Exception ignored) {}
        }

        return "javaw.exe";   // Let's just hope it is in the path :)
    }
    
    static protected String inferTargetKeyNode(String targetExtension,
                                               String internalName) {
        try {
            Pattern pattern = Pattern.compile(
                    "\\s*\\(.+\\)\\s*REG_SZ\\s*(.*)");
            String[] cmd = {"REG", "QUERY", "", "/ve"};
            
            String classKeyNode = "HKCR\\" + targetExtension;
            boolean findAssociatedClass = true;
            while (findAssociatedClass) {
                cmd[2] = classKeyNode;
                List<String> output = new ArrayList<>();
                executeCommand(cmd, output);
                
                findAssociatedClass = false;
                for (String line : output) {
                    Matcher matcher = pattern.matcher(line);
                    if (matcher.find() 
                            && keyNodeExists("HKCR\\" + matcher.group(1))) {
                        classKeyNode = "HKCR\\" + matcher.group(1);
                        findAssociatedClass = true;
                        break;
                    }
                }
            }
            
            classKeyNode = classKeyNode
                    .replaceFirst("HKCR", "HKCU\\\\Software\\\\Classes");
            return classKeyNode + "\\Shell\\" + internalName;
        } catch (Exception x) {
            x.printStackTrace();
        }
        return "";
    }
    
    static protected boolean keyNodeExists(String keyNode) {
        if ((keyNode == null) || (keyNode.trim().length() == 0)) {
            throw new RuntimeException(
                    "Invalid or non-existent Registry-key node !");
        }
        try {
            String[] cmd = {"REG", "QUERY", keyNode};
            return executeCommand(cmd);
        } catch (Exception x) {
            x.printStackTrace();
        }
        return false;
    }
}
